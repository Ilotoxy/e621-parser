# Contributing
If you want to contribute to this project, please create a pull request with every addition you've made.
I will then look over the new additions and check if everything is okay.

Thanks for contributing!