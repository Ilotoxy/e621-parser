# E621 Parser
This project has been made by me in late 2017. 
It has not been made for saving or creating dumps of the productive e621 database(s).
The main idea behind this project was to provide a fallback API for intensive post and tag operations, which developers can use to create bots or other tools (e.g. "smilar post searcher").

How it works: The parser gets a list of all posts on E621 with the proper tag relations for each post and saves them to an internal MySQL database relation.
It does not save any pictures in the database. The only thing it saves is the e621 preview URL and the source of each post.

The API that the parser should provide has not yet been created either, but I will try to create it in another project.