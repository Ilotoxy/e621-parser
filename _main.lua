--
-- Created by IntelliJ IDEA.
-- User: Ilotoxy
-- Date: 17.03.18
-- Time: 11:38
-- To change this template use File | Settings | File Templates.
--

-- Requires
require("utility/cClasslib");
require("utility/cSingleton");
require("utility/cLogger");
require("cPostUpdater");
require("cPostSearcher");
require("cDatabase");
require("cAPI");

-- Classes
require("classes/cPost");
require("classes/cTag");
require("classes/cSearchCondition");
require("classes/cSearchHit");

_sql        = require("luasql.mysql");
_json       = require("cjson");
_https      = require("ssl.https");
_var_dump   = require("utility/var_dump");

--- Begin
local function begin()
    -- Check Database Connection
    cDatabase:getInstance():checkConnection(_sql);
end

--- updateTags
local function updateTags(ab)
    cLogger:getInstance():info("Das dauert mehrere Stunden.");
    return cPostUpdater:getInstance():updateTags();
end

--- updatePosts
local function updatePosts(ab)
    cLogger:getInstance():info("Das kann lange dauern, sehr lange!!");
    return cPostUpdater:getInstance():updatePosts(ab);
end

--- beginPostSearch
local function beginPostSearch()
    cPostSearcher:getInstance():resetConditions();

    local con_1 = cSearchCondition:new();
    con_1:setUser("USERNAME_HERE");

    local conditions = {con_1};

    for index, condition in pairs(conditions) do
        cPostSearcher:getInstance():addCondiction(condition);
    end

    local hits = cPostSearcher:getInstance():search();

    for index, hit in pairs(hits) do
        cLogger:getInstance():info("["..index.."] Gefunden: "..hit:getPost()._id.." ["..hit:getPercentageMatched().."% | "..hit:getExecutionCalls().."]");
    end
end

--- _main()
local function _main()
    begin();
    print("Aktionen:");
    print("Vorhandene Posts Updaten (1), Neue Posts Holen (2), Tags Updaten (3), Suche nach Vorschlägen (4), Alles (1+2) (5)")

    -- Number + Debug
    local number;
    if not(_DEBUG) then
        number = io.read("*n");
    else
        number = 3;
    end

    if not(number) then number = 1; end

    if(number == 1) then
        updatePosts();
    elseif(number == 2) then
        updatePosts(true);
    elseif(number == 3) then
        updateTags();
    elseif(number == 4) then
        beginPostSearch();
    elseif(number == 5) then
        updateTags();
        updatePosts(true);
    end

    -- Zum Schluss: Beenden
    cDatabase:getInstance():destruct();
end

_DEBUG = false;

_main();