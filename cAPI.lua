--
-- Created by IntelliJ IDEA.
-- User: Ilotoxy
-- Date: 17.03.18
-- Time: 12:22
-- To change this template use File | Settings | File Templates.
--

cAPI = inherit(cSingleton);

-- //////////////
-- //// getPostIndex
-- //////////////
function cAPI:getPostIndex(settings)
    return self:_makeIndexRequest("post", settings);
end

-- //////////////
-- //// getTagIndex
-- //////////////
function cAPI:getTagIndex(settings)
    return self:_makeIndexRequest("tag", settings);
end

-- //////////////
-- //// _makeIndexRequest
-- //////////////
function cAPI:_makeIndexRequest(where, settings)
    local subFile = where.."/index.json";

    local arguments = "";

    for index, key in pairs(settings) do
        arguments = arguments.."?&"..index.."="..key;
    end

    local url = self._requestURL .. subFile .. arguments;

    local json = self:makeHTTPRequest("GET", url);
    return json;
end

-- //////////////
-- //// makeHTTPRequest
-- //////////////
function cAPI:makeHTTPRequest(method, url, payload, ...)
    self:waitForNewRequest();
    self:log(method.." "..url);

    local chunks = {};
    local headers = {
        ["user-agent"] = "E621 Recommended Post Searcher/1.0, Lua5.1/Luasec, (by INSERT_NAME on e621)",
    }

    local r, c, h, s = _https.request {
        url = url,
        method = method,
        headers = headers,
        sink = ltn12.sink.table(chunks)
    }

    local response = table.concat(chunks);

    self:log(s..", Length: "..h["content-length"]);
    -- OK?
    if(c == 200 or c == 201 or c == 202 or c == 203) then
        return response;
    else
        self:log("Got bad return code: "..c);
        return false;
    end
end

-- //////////////
-- //// getUserFavorites
-- //////////////
function cAPI:getUserFavorites(sUsername)
    local posts = {};

    local iCurPage = 1;
    while(true) do
        local url = self._requestURL .. "post/index.json?tags=fav:"..sUsername.."&limit="..
                self._iFavGetLimit.."&page="..iCurPage;
        local json = self:makeHTTPRequest("GET", url);

        if(json and #json > 10) then
            json = _json.decode(json);
            iCurPage = iCurPage + 1;
            -- Post Instanz aus Datenbank holen
            for _, post in ipairs(json) do
                local id = post.id;
                local post_instance = cDatabase:getInstance():getPostByID(id);
                table.insert(posts, post_instance);
            end
        else
            break;
        end
    end

    return posts;
end

-- //////////////
-- //// waitForNewRequest
-- //////////////
function cAPI:waitForNewRequest()
    if(self._iLastRequestTime - os.time() > 1) then
        self._iLastRequestTime = os.time();
        return true;
    else
        sleep(1);
    end
end

-- //////////////
-- //// Constructor
-- //////////////
function cAPI:constructor(...)
    --
    self._className = "cAPI";
    self._requestURL = "http://e621.net/";

    -- HTTP
    self._iFavGetLimit = 100; -- Number of max. Limits per Request

    self._iLastRequestTime = 0;
end

-- //////////////
-- //// Destructor
-- //////////////
function cAPI:destruct(...)

end