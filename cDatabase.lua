--
-- Created by IntelliJ IDEA.
-- User: Ilotoxy
-- Date: 17.03.18
-- Time: 11:56
-- To change this template use File | Settings | File Templates.
--

cDatabase = inherit(cSingleton);

-- //////////////
-- //// CheckDatabase
-- //////////////
function cDatabase:checkConnection(sql)
    self._sql = sql;
    self._env = sql.mysql();

    self:log("Checking Database...");
    assert(self._sql and self._env, "SQL var is missing!");

    self._con = self._env:connect(
        self._connectionString["database"],
        self._connectionString["username"],
        self._connectionString["password"],
        self._connectionString["host"]
    );

    assert(self._con, "Can't connect to database!");
    self:log("Database connection OK.");
end

-- //////////////
-- //// escapeString
-- //////////////
function cDatabase:escapeString(str)
    return self:getConnection():escape(str);
end

-- //////////////
-- //// execute
-- //////////////
function cDatabase:execute(query)
    assert(self._con, "Database connection has gone away");

    local query, error = self._con:execute(query);

    if(error) then
        self:log(error);
    end
    assert(query, "Es gab ein Fehler bei der Ausfuehrung eines SQL-Commands.");

    if(query) then
        return query;
    else
        return nil;
    end
end

-- //////////////
-- //// getConnection
-- //////////////
function cDatabase:getConnection()
    return self._con;
end

-- //////////////
-- //// getPostTags
-- //////////////
function cDatabase:getPostTags(cpost)
    local result = self:execute(string.format("SELECT * FROM posts_tags WHERE PostID = '%s';", cpost._id));

    return result;
end

-- //////////////
-- //// getPostsByArtist
-- //////////////
function cDatabase:getPostsByArtist(artistname)
    -- TODO
    return {};
end

-- //////////////
-- //// loadAllTags
-- //////////////
function cDatabase:loadAllTags()
    self:log("Lade alle Tags aus Datenbank. Das kann etwas dauern.");

    local result = self:execute("SELECT * FROM tags;");
    local row = assert(result:fetch({}, "a"));

    -- Jeden Tag durchloopen
    while (row) do
        local ctag = cTag:new(row, true);
        self:addTag(ctag);
        row = result:fetch(row, "a");
    end

    self._iTagsLastUpdated = os.time();

    return self._tblTags;
end

-- //////////////
-- //// loadAllTags
-- //////////////
function cDatabase:loadAllPosts()
    self:log("Noch nicht implementiert! (LoadAllPosts())");
    -- TODO
    --[[
        local result = self:execute("SELECT * FROM posts;");
        local row = assert(result:fetch({}, "a"));
    ]]
    self._iPostsLastUpdated = os.time();

    return nil
end

-- //////////////
-- //// getPostByID
-- //////////////
function cDatabase:getPostByID(post_id)
    local result = self:execute(string.format("SELECT * FROM posts WHERE PostID = '%s';", post_id));
    local row = assert(result:fetch({}, "a"));
    if(row) then
        return cPost:new(row, true);
    end
end


-- //////////////
-- //// getAllTagsByIndexID
-- //////////////
function cDatabase:getAllTagsByIndexID()
    self:makeSureTagsAreUpToDate();
    return self._tblTags;
end

-- //////////////
-- //// getAllTagsByIndexName
-- //////////////
function cDatabase:getAllTagIDsByIndexName()
    self:makeSureTagsAreUpToDate();
    return self._tblTagNames;
end

-- //////////////
-- //// getAllTags
-- //////////////
function cDatabase:getAllTags()
    return self:getAllTagsByIndexID();
end

-- //////////////
-- //// makeSureTagsAreUpToDate
-- //////////////
function cDatabase:makeSureTagsAreUpToDate()
    if(self._iTagsLastUpdated == 0) then
        self:loadAllTags();
    end

    return true;
end
-- //////////////
-- //// makeSurePostsAreUpToDate
-- //////////////
function cDatabase:makeSurePostsAreUpToDate()
    if(self._iPostsLastUpdated == 0) then
        self:loadAllPosts();
    end

    return true;
end

-- //////////////
-- //// addTag
-- //////////////
function cDatabase:addTag(tag)
    self._tblTags[tag._id] = tag;
    self._tblTagNames[tag._name] = tag._id;

    return self._tblTags;
end

-- //////////////
-- //// addPost
-- //////////////
function cDatabase:addPost(post)
    self._tblPosts[post._id] = post;

    return self._tblPosts;
end

-- //////////////
-- //// Constructor
-- //////////////
function cDatabase:constructor(...)
    --
    self._className = "cDatabase";

    -- Vars
    self._connectionString = {
        ["host"] = "localhost",
        ["username"] = "e621",
        ["password"] = "",
        ["database"] = "",
    }

    self._tblTags               = {};
    self._tblTagNames           = {};
    self._tblPosts              = {}; -- Geparste Posts

    self._iTagsLastUpdated      = 0;
    self._iPostsLastUpdated      = 0;
end

-- //////////////
-- //// Destructor
-- //////////////
function cDatabase:destruct(...)
    if(self._con) then
        self._con:close();
    end
    if(self._env) then
        self._env:close();
    end
    self:log("Database connection shutting down.");
end