--
-- Created by IntelliJ IDEA.
-- User: Ilotoxy
-- Date: 18.03.18
-- Time: 19:40
-- To change this template use File | Settings | File Templates.
--


cPostSearcher = inherit(cSingleton);

-- //////////////
-- //// cPostSearcher
-- //////////////
function cPostSearcher:addCondiction(condition)
    table.insert(self._tblConditions, condition);
end

-- //////////////
-- //// removeCondiction
-- //////////////
function cPostSearcher:removeCondiction(condition)
    for index, key in pairs(self._tblConditions) do
        if(key == condition) then
            self._tblConditions[index] = nil;
        end
    end

    return true;
end

-- //////////////
-- //// resetConditions
-- //////////////
function cPostSearcher:resetConditions()
    self._tblConditions = {};
end

-- //////////////
-- //// search
-- //////////////
--[[
    Sucht in der Datenbank nach aehnlichen Posts.
    Dabei werden bestimmte kriterien befolgt.

    - % Uebereinstimmung mit anderen Tags
    - % Uebereinstimmung mit anderen Upvotes / Favorites
    - Sortiert nach Votes / Favoriten
 ]]
function cPostSearcher:search(iHowMany)
    if not(iHowMany) then iHowMany = 10 end
    local found;

    self:log("Beginne Suche. Anzahl Conditions: "..#self._tblConditions);

    -- Alle Conditions durchgehen
    for _, condition in pairs(self._tblConditions) do
        self:log("Condition: "..condition:getShortDescription());
    end

    if(self._bSearchByTags) then
        self:searchByTags();
    end

    found = self._tblHits;

    return found;
end

-- //////////////
-- //// searchByTags
-- //////////////
function cPostSearcher:searchByTags()
    self:log("Suche nach Tags.");

    local applied_tags = {};

    -- Alle Tags aus den Conditions Sammeln
    for _, condition in pairs(self._tblConditions) do
        local condition_tags = condition:getAppliedTags();
        for _, tag in pairs(condition_tags) do
            table.insert(applied_tags, tag);
        end
    end

    -- Doppelte Tags raussortieren
    local fixed_tags = {}
    local tag_ammount = {};

    for _, tag in pairs(applied_tags) do
        local tag_id = tag._id;
        if not(fixed_tags[tag_id]) then
            fixed_tags[tag_id] = tag;
            tag_ammount[tag_id] = 1;
        else
            tag_ammount[tag_id] = tag_ammount[tag_id] + 1;
        end
    end

    local iArray    = {}
    local iLast     = 1;

    for id, amount in pairs(tag_ammount) do
        iArray[iLast] = {id, amount, fixed_tags[id]._name};
        iLast = iLast + 1;
    end

    self:log("Es wurden "..iLast.." unterschiedliche Tags gefunden.");

    -- Bubblesort
    for _, _ in pairs(tag_ammount) do
        for i = 1, iLast, 1 do
            if(iArray[i + 1]) then
                if(iArray[i][2] < iArray[i + 1][2]) then
                    local biggerArray = iArray[i + 1];
                    iArray[i + 1] = iArray[i];
                    iArray[i] = biggerArray;
                end
            end
        end
    end

    self:log("Die 10 meisten Tags der Conditions:");

    for i = 1, 10, 1 do
        self:log(iArray[i][1]..": "..iArray[i][3].." ("..iArray[i][2].."x)");
    end
end

function cPostSearcher:getSmilarPostsByTags(tags)
    -- Todo
end


-- //////////////
-- //// constructor
-- //////////////
function cPostSearcher:constructor(...)
    --
    self._className             = "cPostSearcher";
    self._tblConditions         = {};
    self._tblHits               = {};

    self._bSearchByTags         = true;
end