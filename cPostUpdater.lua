--
-- Created by IntelliJ IDEA.
-- User: Ilotoxy
-- Date: 17.03.18
-- Time: 11:43
-- To change this template use File | Settings | File Templates.
--

cPostUpdater = inherit(cSingleton);

-- //////////////
-- //// updatePosts
-- //////////////
function cPostUpdater:updatePosts(bInsertNewOne)
    self:log("Updating Posts...");

    if not(bInsertNewOne) then
        bInsertNewOne = false;
    end

    -- Letzte POST ID bekommen
    local lastID = 0;

    do
        local result = cDatabase:getInstance():execute("SELECT PostID FROM posts ORDER BY PostID DESC LIMIT 1;");

        local row = result:fetch({}, "a");

        if(row == nil) then
            self:log("POSTS] Keine LastID in der Datenbank gefunden. Suche auf Website...");
            lastID = self:getLastPostID();
        else
            if not(bInsertNewOne) then
                lastID = row.PostID;
            else
                self:log("Hole neue Posts von e621. (Override!)");
                lastID = self:getLastPostID();
            end
        end
    end
    self:log("POSTS] LastID gefunden: "..lastID);

    -- Die letzen Posts erhalten, und in die Datenbank updaten
    local _lastID = lastID;
    local iCount = 0;

    while(true) do
        self:log("Erhalte "..self._iMaxPostsPerRequest.." posts abwärts ab ID: "..lastID);
        local posts = cAPI:getInstance():getPostIndex(
            {
                ["limit"] = self._iMaxPostsPerRequest,
                ["before_id"] = lastID,
            }
        );
        posts = _json.decode(posts);

        if(posts) then
            for index, post in ipairs(posts) do
                local postid = post["id"];

                self:log("Post ["..postid.."]["..iCount.."] wird bearbeitet...");

                local cpost = cPost:new(post);

                cDatabase:getInstance():addPost(cpost)

                self:checkPostAndUpdate(cpost);
                iCount = iCount+1;

                lastID = postid;
            end
        end

        if(_lastID == lastID) then
            self:log("POSTS] Error: Abbruch durch keine Aenderung von LastID!");
            break;
        end
        self:log("[POSTS] Warte "..self._iWaitTime.." Sekunden, um weitere Posts zu speichern...");
        sleep(5);
    end

    self:log("Posts updated.");
end

-- //////////////
-- //// updateTags
-- //////////////
function cPostUpdater:updateTags()
    self:log("Updating Tags...");

    -- Die letzen Posts erhalten, und in die Datenbank updaten
    local iCount = 0;
    local iPageCount = self._iTagStartPage;

    while(true) do
        self:log("Erhalte "..self._iMaxTagsPerRequest.." tags abwärts ab Seite: "..iPageCount);
        local tags = cAPI:getInstance():getTagIndex(
            {
                ["limit"] = self._iMaxTagsPerRequest,
                ["order"] = "date",
                ["page"] = iPageCount;
            }
        );
        tags = _json.decode(tags);

        if(tags and #tags >= 1) then
            for index, tag in ipairs(tags) do
                local tagid = tag["id"];

                self:log("Tag ["..tagid.."] wird bearbeitet...");

                local ctag = cTag:new(tag);
                cDatabase:getInstance():addTag(ctag);
                self:checkTagAndUpdate(ctag);
                iCount = iCount+1;
            end
        else
            self:log("[TAGS] Error: Abbruch durch zu geringe Tag results!");
            break;
        end
        iPageCount = iPageCount + 1;

        self:log("[TAGS] Warte "..self._iWaitTime.." Sekunden, um weitere Tags zu speichern...");
        sleep(self._iWaitTime);
    end

    -- Tags geupdated
    cDatabase:getInstance()._iTagsLastUpdated = os.time();

    return true;
end

-- //////////////
-- //// checkPostAndUpdate
-- //////////////
function cPostUpdater:checkPostAndUpdate(post)
    local databaseEntry = post:getDatabaseEntry();

    if not(databaseEntry) then
        self:log("[INSERT]: "..post._id);
        post:insertIntoDatabase();
    else
        self:log("[UPDATE]: "..post._id);
        post:updateInDatabase();
    end
end

-- //////////////
-- //// checkTagAndUpdate
-- //////////////
function cPostUpdater:checkTagAndUpdate(tag)
    local dbEntry = tag:getDatabaseEntry();

    if not(dbEntry) then
        self:log("[INSERT]: "..tag._id..", "..tag._name);
        tag:insertIntoDatabase();
    else
        if(dbEntry.Count ~= tag._count) then
            self:log("[UPDATE]: "..tag._id..", "..tag._name);
            tag:updateDatabaseEntry();
        end
    end
end

-- //////////////
-- //// getLastPostID
-- //////////////
function cPostUpdater:getLastPostID()
    local jsonPost = cAPI:getInstance():getPostIndex({["limit"] = 1});
    -- self:log(jsonPost);
    local post = _json.decode(jsonPost);

    return post[1]["id"];
end


-- //////////////
-- //// constructor
-- //////////////
function cPostUpdater:constructor(...)
    --
    self._className             = "cPostUpdater";
    self._iMaxPostsPerRequest   = 320; -- Maximale anzahl an Posts per Request;
    self._iMaxTagsPerRequest    = 500;
    self._iTagStartPage         = 1;

    self._iWaitTime             = 5;
end