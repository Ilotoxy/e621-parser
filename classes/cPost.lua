--
-- Created by IntelliJ IDEA.
-- User: Ilotoxy
-- Date: 17.03.18
-- Time: 14:08
-- To change this template use File | Settings | File Templates.
--


cPost = {};

-- //////////////
-- //// new
-- //////////////
function cPost:new(...)
    local obj = setmetatable({}, {__index = self});
    if obj.constructor then
        obj:constructor(...);
    end
    return obj;
end

function cPost:log(message, ...)
    return cLogger:getInstance():log("cPost", "["..self._id.."] "..message, ...);
end

-- //////////////
-- //// parse
-- //////////////
function cPost:parse()
    assert(self._tblDecodedJson, "self not existing");

    if not(self._bIsDatabaseEntry) then
        for index, key in pairs(self._tblDecodedJson) do
            if(key ~= nil) then
                if(type(key) == "table") then
                    self["_"..index] = tostring(table.concat(key, ", "));
                else
                    self["_"..index] = tostring(key);
                end
            end
        end
    else
        for db_index, json_index in pairs(self._tblDBRelations) do
            local value = self._tblDecodedJson[db_index];
            if (value ~= nil) then
                self["_"..json_index] = value;
            end
        end
    end

    if(self._source:find("userdata") and self._source:find("nil")) then
        self._source = nil;
    end

    local all_tags          = cDatabase:getInstance():getAllTagsByIndexID();
    local all_tags_by_name  = cDatabase:getInstance():getAllTagIDsByIndexName();

    if not(self._bIsDatabaseEntry) then
        -- Tags
        for tag in self._tags:gmatch("%S+") do
            tag = cDatabase:getInstance():escapeString(tag);
            local tag_instance = all_tags[all_tags_by_name[tag]];

            -- Existiert der Tag oder nicht?
            if not(tag_instance) then
                self:log("ERROR: Tag nicht gefunden! Bitte Tags aktualisieren. Tagname: "..tag, "error");
            else
                self:addTag(tag_instance);
            end
        end
    else
        local result = cDatabase:getInstance():getPostTags(self);
        local row = result:fetch({}, "a");

        while(row) do
            local tag_instance = all_tags[row['TagID']];

            if(tag_instance) then
                self:addTag(tag_instance);
            else
                self:log("Error: Tag Intanz nicht in globale Tag-Tabelle gfunden: "..row['TagID'], "error");
            end
            row = result:fetch({}, "a");
        end
    end
end

-- //////////////
-- //// insertIntoDatabase
-- //////////////
function cPost:insertIntoDatabase()

    local function insertPost()
        local query = "INSERT INTO posts (PostID, Description, CreatorID, Author, Source, MD5, PreviewURL, Rating, Score, Favorites, Changed, Artist)";
        query = query .. " VALUES ";
        query = query ..string.format("('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');",
            self._id,
            cDatabase:getInstance():escapeString(self._description or "NULL"),
            self._creator_id or "NULL",
            cDatabase:getInstance():escapeString(self._author or "NULL"),
            cDatabase:getInstance():escapeString(self._source or "NULL"),
            tostring(self._md5),
            cDatabase:getInstance():escapeString(self._preview_url or "NULL"),
            self._rating or "e",
            self._score or 0,
            self._fav_count or 0,
            self._change or nil,
            self:getArtistString() or nil);

        cDatabase:getInstance():execute(query);
    end

    insertPost();
    self:addTagRelations();
end

-- //////////////
-- //// addTagRelations
-- //////////////
function cPost:addTagRelations()
    self:log("Füge Tag-Relationen hinzu.");

    -- all_tags = table instanceof(cTag)
    for tag_id, tag_instance in pairs(self:getTags()) do
        -- Hinzufuegen in relationstabelle
        local query = "INSERT INTO posts_tags VALUES ('%i', '%i')";
        query = query:format(self._id, tag_id);

        cDatabase:getInstance():execute(query);
        self:log("Tag: "..tag_instance._name.." ["..tag_id.."] hinzugefügt", "debug");
    end
end

-- //////////////
-- //// updateInDatabase
-- //////////////
function cPost:updateInDatabase()
    -- wird aufgerufen, wenn der Post schon existiert.
    -- Was aktualisieren: Post-Metadaten, Tags (Alte entfernen, neue hinzufuegen)

    local function updateMetadata()
        local query = string.format("UPDATE posts SET Description = '%s', CreatorID = '%s', Author = '%s', Source = '%s', MD5 = '%s', PreviewURL = '%s', Rating = '%s', Score = '%s', Favorites = '%s', Changed = '%s', LastUpdated = CURRENT_TIMESTAMP(), Artist = '%s' WHERE PostID = '%s'",
            cDatabase:getInstance():escapeString(self._description or "NULL"),
            self._creator_id or "NULL",
            cDatabase:getInstance():escapeString(self._author or "NULL"),
            cDatabase:getInstance():escapeString(self._source or "NULL"),
            tostring(self._md5),
            cDatabase:getInstance():escapeString(self._preview_url or "NULL"),
            self._rating or "e",
            self._score or 0,
            self._fav_count or 0,
            self._change or nil,
            self:getArtistString() or nil,
            self._id
        );

        cDatabase:getInstance():execute(query);
    end

    local function removeOldTags()
        self:log("Lösche alte Tag-Relationen.");
        local query = string.format("DELETE FROM posts_tags WHERE PostID = '%i';", self._id);
        cDatabase:getInstance():execute(query);
    end

    local function addNewTags()
        self:addTagRelations();
    end

    updateMetadata();
    removeOldTags();
    addNewTags();
end

-- //////////////
-- //// getDatabaseResult
-- //////////////
function cPost:getDatabaseEntry()
    local query = string.format("SELECT * FROM posts WHERE PostID = '%s'", self._id);

    local result = cDatabase:getInstance():execute(query);
    local row = result:fetch({}, "a");

    return row;
end

-- //////////////
-- //// getTags
-- //////////////
function cPost:getTags()
    return self._tblTags;
end

-- //////////////
-- //// addTag
-- //////////////
function cPost:addTag(ctag)
    self._tblTags[ctag._id] = ctag;

    return ctag;
end

-- //////////////
-- //// removeTag
-- //////////////
function cPost:removeTag(ctag)
    self._tblTags[ctag._id] = nil;

    return true;
end

-- //////////////
-- //// getArtistString
-- //////////////
function cPost:getArtistString()
    if(self._artist ~= nil) then
        if(type(self._artist) == "table") then
            return cDatabase:getInstance():escapeString(table.concat(self._artist, ", "));
        else
            return cDatabase:getInstance():escapeString(self._artist);
        end
    end
end

-- //////////////
-- //// constructor
-- //////////////
function cPost:constructor(decodedJsonData, bIsDatabase)
    --
    self._tblDecodedJson = decodedJsonData;
    self._tblTags = {};

    self._tblDBRelations = {
        ["PostID"]      = "id",
        ["Description"] = "description",
        ["Author"]      = "author",
        ["Source"]      = "source",
        ["MD5"]         = "md5",
        ["PreviewURL"]  = "preview_url",
        ["Rating"]      = "rating",
        ["Score"]       = "score",
        ["Changed"]     = "changed",
        ["LastUpdated"] = "last_updated",
    }

    self._bIsDatabaseEntry = false;

    if(bIsDatabase) then
        self._bIsDatabaseEntry = true;
    end

    self:parse();
end