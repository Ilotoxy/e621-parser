--
-- Created by IntelliJ IDEA.
-- User: Ilotoxy
-- Date: 18.03.18
-- Time: 19:37
-- To change this template use File | Settings | File Templates.
--

cSearchCondition = {};

-- //////////////
-- //// new
-- //////////////
function cSearchCondition:new(...)
    local obj = setmetatable({}, {__index = self});
    if obj.constructor then
        obj:constructor(...);
    end
    return obj;
end

-- //////////////
-- //// log
-- //////////////
function cSearchCondition:log(message, ...)
    return cLogger:getInstance():log("cSearchCondition", message, ...);
end

--[[
-- //////////////
-- //// addTag
-- //////////////
function cSearchCondition:addTag(tag)
    self._tblTags[tag._id] = tag;
end

-- //////////////
-- //// addPost
-- //////////////
function cSearchCondition:addPost(post)
    self._tblPosts[post._id] = post;
end

-- //////////////
-- //// removePost
-- //////////////
function cSearchCondition:removeTag(tag)
    self._tblTags[tag._id] = nil;
end

-- //////////////
-- //// removePost
-- //////////////
function cSearchCondition:removePost(post)
    self._tblPosts[post._id] = nil;
end

]]
-- //////////////
-- //// setPost
-- //////////////
function cSearchCondition:setPost(cpost)
    self._post = cpost;
end

-- //////////////
-- //// setTag
-- //////////////
function cSearchCondition:setTag(ctag)
    self._tag = ctag;
end

-- //////////////
-- //// setArtist
-- //////////////
function cSearchCondition:setArtist(artistName)
    self._artist = artistName;
end

-- //////////////
-- //// setUser
-- //////////////
function cSearchCondition:setUser(userName)
    self._user = userName;
end

-- //////////////
-- //// getShortDescription
-- //// Returns a short description about the search condition.
-- //////////////
function cSearchCondition:getShortDescription()
    local str = "";

    if(self._post ~= nil) then
        str = str .. "Post: ("..self._post._id..") ";
    end

    if(self._tag ~= nil) then
        str = str .. "Tag: ("..self._tag._id.." / "..self._tag._name..") ";
    end

    if(self._artist ~= nil) then
        str = str .. "Artist: ("..self._artist..") ";
    end

    if(self._user ~= nil) then
        str = str .. "User Favs: ("..self._user..") ";
    end

    return str;
end

-- //////////////
-- //// getAppliedTags
-- //// Sucht nach den Tags einer Search Condition.
-- //// Bei Usern: Posts -> Tags
-- //// Bei Posts: -> Tags
-- //// Bei Artists: Posts -> Tags
-- //// Bei Tags: -> Tags
-- //////////////
function cSearchCondition:getAppliedTags()
    local tags = {};

    -- Tag
    if(self._tag ~= nil) then
        table.insert(tags, self._tag);
    end

    -- Post
    if(self._post ~= nil) then
        for _, tag in pairs(self._post:getTags()) do
            table._insert(tags, tag);
        end
    end

    -- Artist
    if(self._artist ~= nil) then
        local artist_posts = cDatabase:getInstance():getPostsByArtist(self._artist);

        for _, post in pairs(artist_posts) do
            for _, tag in pairs(post:getTags())  do
                table.insert(tags, tag);
            end
        end
    end

    -- User
    if(self._user ~= nil) then
        local user_posts = cAPI:getInstance():getUserFavorites(self._user);

        for _, post in pairs(user_posts) do
            for _, tag in pairs(post:getTags()) do
                --_var_dump(tag);
                table.insert(tags, tag);
            end
        end
    end

    return tags;
end

-- //////////////
-- //// constructor
-- //////////////
function cSearchCondition:constructor()
    self._post      = nil;
    self._tag       = nil;
    self._artist    = nil;
    self._user      = nil;
end