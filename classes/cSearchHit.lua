--
-- Created by IntelliJ IDEA.
-- User: Ilotoxy
-- Date: 19.03.18
-- Time: 14:45
-- To change this template use File | Settings | File Templates.
--

cSearchHit = {};

-- //////////////
-- //// new
-- //////////////
function cSearchHit:new(...)
    local obj = setmetatable({}, {__index = self});
    if obj.constructor then
        obj:constructor(...);
    end
    return obj;
end

-- //////////////
-- //// log
-- //////////////
function cSearchHit:log(message, ...)
    return cLogger:getInstance():log("[cSearchHit]["..self._post._id.."] "..message, ...);
end

-- //////////////
-- //// setPost
-- //////////////
function cSearchHit:setPost(cpost)
    self._post = cpost;
end

-- //////////////
-- //// getPost
-- //////////////
function cSearchHit:getPost()
    return self._post;
end
-- //////////////
-- //// setPercentageMatched
-- //////////////
function cSearchHit:setPercentageMatched(iMatch)
    self._iPercentageMatched = iMatch;
end

-- //////////////
-- //// getPercentageMatched
-- //////////////
function cSearchHit:getPercentageMatched()
    return self._iPercentageMatched;
end

-- //////////////
-- //// setExecutionCalls
-- //////////////
function cSearchHit:setExecutionCalls(iCalls)
    self._iExecutionCalls = iCalls;
end

-- //////////////
-- //// getExecutionCalls
-- //////////////
function cSearchHit:getExecutionCalls()
    return self._iExecutionCalls;
end
-- //////////////
-- //// constructor
-- //////////////
function cSearchHit:constructor()
    self._post = nil;

    self._iPercentageMatched = 0;
    self._iExecutionCalls    = 0;
end