--
-- Created by IntelliJ IDEA.
-- User: Ilotoxy
-- Date: 17.03.18
-- Time: 14:08
-- To change this template use File | Settings | File Templates.
--

cTag = {};

-- //////////////
-- //// new
-- //////////////
function cTag:new(...)
    local obj = setmetatable({}, {__index = self});
    if obj.constructor then
        obj:constructor(...);
    end
    return obj;
end
function cTag:log(message, ...)
    return cLogger:getInstance():log("cTag", "["..self._id.."] "..message, ...);
end

-- //////////////
-- //// getDatabaseEntry
-- //////////////
function cTag:getDatabaseEntry()
    local query = string.format("SELECT * FROM tags WHERE TagID = '%s'", self._id);

    local result = cDatabase:getInstance():execute(query);
    local row = result:fetch({}, "a");

    return row;
end

-- //////////////
-- //// updateDatabaseEntry
-- //////////////
function cTag:updateDatabaseEntry()
    local query = string.format("UPDATE tags SET Count = '%s' WHERE TagID = '%s'", self._count, self._id);

    cDatabase:getInstance():execute(query);
end


-- //////////////
-- //// insertIntoDatabase
-- //////////////
function cTag:insertIntoDatabase()
    local function insertTag()
        local query = "INSERT INTO tags (TagID, Name, Count, Type)";
        query = query .. " VALUES ";
        query = query ..string.format("('%s', '%s', '%s', '%s');",
            self._id,
            cDatabase:getInstance():escapeString(self._name or "NULL"),
            self._count or "NULL",
            self._type or "NULL");

        cDatabase:getInstance():execute(query);
    end

    insertTag();
end

-- //////////////
-- //// parse
-- //////////////
function cTag:parse()
    assert(self._tblDecodedJson, "self not existing");

    if not(self._bIsDatabaseEntry) then
        for index, key in pairs(self._tblDecodedJson) do
            if(key ~= nil) then
                self["_"..index] = tostring(key);
            end
        end
    else
        for db_index, json_index in pairs(self._tblDBRelations) do
            local value = self._tblDecodedJson[db_index];
            if (value ~= nil) then
                self["_"..json_index] = value;
            end
        end
    end
end


-- //////////////
-- //// constructor
-- //////////////
function cTag:constructor(decodedJsonData, bIsDatabase)
    --
    self._tblDecodedJson = decodedJsonData;

    self._bIsDatabaseEntry = false;
    --[[ctag._id        = row["TagID"];
    ctag._name      = row["Name"];
    ctag._type      = row["Type"];
    ctag._count     = row["Count"];]]

    self._tblDBRelations = {
        ["TagID"] = "id",
        ["Name"] = "name",
        ["Type"] = "type",
        ["Count"] = "count",
    }

    if(bIsDatabase) then
        self._bIsDatabaseEntry = true;
    end

    self:parse();
end