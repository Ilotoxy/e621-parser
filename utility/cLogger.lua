--
-- Created by IntelliJ IDEA.
-- User: Ilotoxy
-- Date: 21.03.18
-- Time: 09:21
-- To change this template use File | Settings | File Templates.
--

cLogger = inherit(cSingleton);

-- //////////////
-- //// openFiles
-- //////////////
function cLogger:openFiles()
    --
    self._files = {
        ["error"] = io.open(self._logDirectory.."error.log", "a+"),
        ["info"] = io.open(self._logDirectory.."info.log", "a+"),
        ["debug"] = io.open(self._logDirectory.."debug.log", "a+"),
    }
end

-- //////////////
-- //// info
-- //////////////
function cLogger:info(message, class)
    return self:log(class, message, "info");
end

-- //////////////
-- //// error
-- //////////////
function cLogger:error(message, class)
    return self:log(class, message, "error");
end

-- //////////////
-- //// debug
-- //////////////
function cLogger:debug(message, class)
    return self:log(class, message, "debug");
end

-- //////////////
-- //// log
-- //////////////
function cLogger:log(class, message, where)
    if not(where) then where = "info" end;
    if not(class) then class= "main" end;

    assert(message ~= nil);

    print("["..string.upper(where).."]["..class.."] "..message);

    if not(self._files[where]) then
        print("Warning: Log file not existing!!! "..where);
    end

    self._files[where]:seek("end");
    self._files[where]:write("["..os.date("%d.%m.%y %H:%M:%S", os.time()).."]["..class.."] "..message.."\n");
end

-- //////////////
-- //// constructor
-- //////////////
function cLogger:constructor(...)
    -- Vars
    self._logDirectory = "logs/"..os.date("%d-%m-%y", os.time()).."/";
    os.execute("mkdir "..self._logDirectory);

    self._files = {};

    self:openFiles();
end

-- //////////////
-- //// destructor
-- //////////////
function cLogger:destructor()
    for _, file in pairs(self._files) do
        file:flush();
        file:close();
    end
end